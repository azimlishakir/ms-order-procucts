package products.order.controller

import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import products.order.service.OrdersService
import spock.lang.Specification

class OrdersControllerTest extends Specification {

    OrdersService ordersService
    OrdersController ordersController
    MockMvc mockMvc

    String url = "/orders"

    def setup() {
        ordersService = Mock()
        ordersController = new OrdersController(ordersService)
        mockMvc = MockMvcBuilders.standaloneSetup(ordersController).setControllerAdvice()
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build()
    }

    def "createOrder"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content("{\n" +
                        "    \"comment\": \"I ordered book 1ww\",\n" +
                        "    \"userId\": 4\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * ordersService.createOrder(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "getOrders"() {
        given:
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response

        then:
        1 * ordersService.getOrders(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "receiverOrder"() {
        given:
        Long orderId = 1

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.put(url.concat("/").concat(
                orderId as String))
                .content("{\n" +
                        "    \"userId\": 2,\n" +
                        "    \"status\": \"REJECT\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * ordersService.receiverOrder(*_)
        assert response.status == HttpStatus.OK.value()
    }

}

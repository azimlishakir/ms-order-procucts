package products.order.controller

import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import products.order.service.UsersService
import spock.lang.Specification

class UsersControllerTest extends Specification {

    UsersService usersService
    UsersController usersController
    MockMvc mockMvc

    String url = "/users"

    def setup() {
        usersService = Mock()
        usersController = new UsersController(usersService)
        mockMvc = MockMvcBuilders.standaloneSetup(usersController).setControllerAdvice()
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build()
    }


    def "getUsers"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .param("page", "0")
                .param("size","10")).andReturn().response

        then:
        1 * usersService.getUsers(*_)
        assert response.status == HttpStatus.OK.value()
    }

    def "CreateUser"() {
        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content("{\n" +
                        "    \"name\": \"Business Vocabulary in use\",\n" +
                        "    \"userId\": 1\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * usersService.createUser(*_)
        assert response.status == HttpStatus.OK.value()

    }

    def "UpdateUser"() {

        given:
        Long id = 1

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.put(url.concat("/").concat(
                id as String))
                .content("{\n" +
                        "    \"username\": \"admin\",\n" +
                        "    \"password\": \"123456\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * usersService.updateUser(*_)
        assert response.status == HttpStatus.OK.value()

    }

    def "deleteUser"() {
        given:
        Long id = 1

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.delete(url.concat("/").concat(
                id as String))
                .contentType(MediaType.APPLICATION_JSON)).andReturn().response
        then:
        1 * usersService.deleteUser(id)
        assert response.status == HttpStatus.OK.value()
    }
}

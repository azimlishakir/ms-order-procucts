package products.order.service.impl


import products.order.dao.OrdersRepository
import products.order.dao.UsersRepository
import products.order.dao.model.Orders
import products.order.dao.model.Users
import products.order.exception.BadRequestException
import products.order.exception.ErrorCode
import products.order.model.OrderStatus
import products.order.model.Role
import products.order.model.filter.OrderFilter
import products.order.model.requests.OrderActionRequest
import products.order.model.requests.OrdersRequest
import products.order.service.OrdersService
import spock.lang.Specification

import java.time.LocalDateTime

class OrdersServiceImplTest extends Specification {

    OrdersRepository ordersRepository
    UsersRepository usersRepository
    OrdersService ordersService

    void setup() {
        ordersRepository = Mock()
        usersRepository = Mock()
        ordersService = new OrdersServiceImpl(ordersRepository, usersRepository)
    }

    def "CreateOrder"() {

        given:
        def userId = 1L

        Users users = Users.builder()
                .id(userId)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.EMPLOYEE)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()

        Orders orders = Orders.builder()
                .id(1L)
                .comment("this book")
                .status(OrderStatus.ORDERED)
                .users(users)
                .build()

        OrdersRequest request = OrdersRequest.builder()
                .comment("this book")
                .userId(1)
                .build()

        when:
        def response = ordersService.createOrder(request)

        then:
        1 * usersRepository.findById(userId) >> Optional.of(users)
        1 * ordersRepository.save(*_) >> orders
        response.getComment() == request.getComment()
    }

    def "CreateOrder failed"() {

        given:
        def userId = 1L

        Users users = Users.builder()
                .id(userId)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.USER)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()

        OrdersRequest request = OrdersRequest.builder()
                .comment("this book")
                .userId(1)
                .build()

        when:
        ordersService.createOrder(request)

        then:
        1 * usersRepository.findById(userId) >> Optional.of(users)
        def e = thrown(BadRequestException)
        assert e.getCode() == ErrorCode.UNEXPECTED_INTERNAL_ERROR
        assert e.getMessage() == "You can not create order."
    }

    def "ReceiverOrder"() {

        def orderId = 5L
        def userId = 4L
        OrderActionRequest request = OrderActionRequest.builder()
                .userId(1)
                .status(OrderStatus.COMPLETED)
                .build()

        Users users = Users.builder()
                .id(1)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.USER)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()


        Orders orders = Orders.builder()
                .id(orderId)
                .comment("this book")
                .status(OrderStatus.ORDERED)
                .users(users)
                .build()


        when:
        ordersService.receiverOrder(request, orderId)

        then:
        1 * ordersRepository.findById(orderId) >> Optional.of(orders)
        1 * usersRepository.findById(request.getUserId()) >> Optional.of(users)
        1 * ordersRepository.save(*_) >> orders
    }

    def "ReceiverOrder COMPLETED failed"() {

        def orderId = 5L
        OrderActionRequest request = OrderActionRequest.builder()
                .userId(1)
                .status(OrderStatus.COMPLETED)
                .build()

        Users users = Users.builder()
                .id(1)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.EMPLOYEE)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()


        Orders orders = Orders.builder()
                .id(orderId)
                .comment("this book")
                .status(OrderStatus.ORDERED)
                .users(users)
                .build()


        when:
        ordersService.receiverOrder(request, orderId)

        then:
        1 * ordersRepository.findById(orderId) >> Optional.of(orders)
        1 * usersRepository.findById(request.getUserId()) >> Optional.of(users)
        def e = thrown(BadRequestException)
        assert e.getCode() == ErrorCode.UNEXPECTED_INTERNAL_ERROR
        assert e.getMessage() == "You can not completed"

    }

    def "ReceiverOrder REJECT wit user role failed"() {

        def orderId = 5L
        OrderActionRequest request = OrderActionRequest.builder()
                .userId(1)
                .status(OrderStatus.REJECT)
                .build()

        Users users = Users.builder()
                .id(1)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.USER)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()


        Orders orders = Orders.builder()
                .id(orderId)
                .comment("this book")
                .status(OrderStatus.ORDERED)
                .users(users)
                .build()


        when:
        ordersService.receiverOrder(request, orderId)

        then:
        1 * ordersRepository.findById(orderId) >> Optional.of(orders)
        1 * usersRepository.findById(request.getUserId()) >> Optional.of(users)

    }

    def "ReceiverOrder reject with EMPLOYEE role failed"() {

        def orderId = 5L
        OrderActionRequest request = OrderActionRequest.builder()
                .userId(1)
                .status(OrderStatus.REJECT)
                .build()

        Users users = Users.builder()
                .id(1)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.EMPLOYEE)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()


        Orders orders = Orders.builder()
                .id(orderId)
                .comment("this book")
                .status(OrderStatus.ORDERED)
                .users(users)
                .build()


        when:
        ordersService.receiverOrder(request, orderId)

        then:
        1 * ordersRepository.findById(orderId) >> Optional.of(orders)
        1 * usersRepository.findById(request.getUserId()) >> Optional.of(users)
        def e = thrown(BadRequestException)
        assert e.getCode() == ErrorCode.UNEXPECTED_INTERNAL_ERROR
        assert e.getMessage() == "You can not reject"

    }

    def "GetOrders"() {
        given:
        def userId = 1L
        OrderFilter request = OrderFilter.builder()
                .id(2L)
                .userId(userId)
                .status(OrderStatus.ORDERED)
                .build()

        Users users = Users.builder()
                .id(userId)
                .name("admin")
                .lastName("admin")
                .userName("admin")
                .password("123456")
                .role(Role.EMPLOYEE)
                .createdAt(LocalDateTime.now().minusDays(10))
                .updatedAt(LocalDateTime.now())
                .build()


        Orders orders = Orders.builder()
                .id(1L)
                .comment("this book")
                .status(OrderStatus.ORDERED)
                .users(users)
                .build()

        when:
        ordersService.getOrders(request)

        then:
        1 * ordersRepository.findAll(*_) >> List.of(orders)
    }
}

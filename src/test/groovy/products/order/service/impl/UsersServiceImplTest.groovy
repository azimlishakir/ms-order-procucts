package products.order.service.impl

import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.security.crypto.password.PasswordEncoder
import products.order.dao.UsersRepository
import products.order.dao.model.Users
import products.order.model.Role
import products.order.model.requests.UsersRequest
import products.order.service.UsersService
import spock.lang.Specification

import java.time.LocalDateTime

class UsersServiceImplTest extends Specification {

    UsersRepository usersRepository
    UsersService usersService
    PasswordEncoder passwordEncoder

    def setup() {
        usersRepository = Mock()
        passwordEncoder = Mock(PasswordEncoder)
        usersService = new UsersServiceImpl(usersRepository, passwordEncoder)
    }


    def "GetUsers"() {
        given:
        Users users = Users.builder()
                .id(1L)
                .name("Shakir")
                .lastName("Azimli")
                .userName("shakir.azimli")
                .password("123456")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build()

        UsersRequest request = UsersRequest.builder()
                .name("Shakir")
                .lastName("Azimli")
                .userName("shakir.azimli")
                .password("123456")
                .role(Role.USER)
                .build()

        when:
        def response = usersService.getUsers(request, PageRequest.of(0, 10))
        then:
        1 * usersRepository.findAll(*_)  >> new PageImpl(List.of(users), PageRequest.of(0, 10), 10)
        1 * response.get(0).getId() == users.getId()
    }


    def "CreateUser"() {
        def id = 1
        Users users = Users.builder()
                .id(1L)
                .userName("test")
                .password("123")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build()

        def request = UsersRequest.builder()
                .id(id)
                .userName(users.getUsername())
                .password("123456")
                .build()

        String decode = "TcG13KOZRSuTNiLyoutpXq8h7CZhMIVDfrYn.f/sX2"


        when:
        def response = usersService.createUser(request)
        then:
        1 * passwordEncoder.encode(request.getPassword()) >> decode
        1 * usersRepository.save(*_) >> users
        1 * response.getId() == users.getId()
    }

    def "UpdateUser"() {

        def id = 1
        Users users = Users.builder()
                .id(1L)
                .userName("test")
                .password("123")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build()

        def request = UsersRequest.builder()
                .id(id)
                .userName(users.getUsername())
                .password("123456")
                .build()

        String decode = "TcG13KOZRSuTNiLyoutpXq8h7CZhMIVDfrYn.f/sX2"


        when:
        def response = usersService.updateUser(request, id)
        then:
        1 * usersRepository.findById(id) >> Optional.of(users)
        1 * passwordEncoder.encode(request.getPassword()) >> decode
        1 * usersRepository.save(*_) >> users
        1 * response.getId() == users.getId()
    }

    def "DeleteUser"() {
        def id = 1
        Users users = Users.builder()
                .id(1L)
                .userName("test")
                .password("123")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build()
        when:
        usersService.deleteUser(id)
        then:
        1 * usersRepository.findById(id) >> Optional.of(users)
    }

    def "DeleteUser fail"() {
        def id = null
        Users users = Users.builder()
                .userName("test")
                .password("123")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build()
        when:
        usersService.deleteUser(id)
        then:
        1 * usersRepository.findById(id) >> Optional.of(users)
        def e = thrown(IllegalArgumentException)
        assert e.getMessage() == "User not found"
    }

    def "LoadUserByUsername"() {

        def userName = "shakir.azimli"
        Users users = Users.builder()
                .id(1L)
                .userName(userName)
                .password("123")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build()
        when:
        usersService.loadUserByUsername(userName)
        then:
        1 * usersRepository.findByUserName(userName) >> Optional.of(users)
    }
}

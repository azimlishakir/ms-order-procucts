package products.order.exception;

public class NotFoundException extends CustomException {
    public NotFoundException(String code, String message) {
        super(code, message);
    }
}


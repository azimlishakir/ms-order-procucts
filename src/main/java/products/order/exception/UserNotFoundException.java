package products.order.exception;

public class UserNotFoundException extends CustomException {
    public UserNotFoundException(String code, String message) {
        super(code, message);
    }
}

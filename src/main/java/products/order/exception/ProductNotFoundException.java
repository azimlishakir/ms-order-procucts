package products.order.exception;

public class ProductNotFoundException extends CustomException {
    public ProductNotFoundException(String code, String message) {
        super(code, message);
    }
}


package products.order.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;
import products.order.dao.model.Orders;
import products.order.model.requests.OrdersRequest;
import products.order.model.responses.OrdersResponse;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrdersMapper {

    OrdersMapper INSTANCE = Mappers.getMapper(OrdersMapper.class);

    @Mapping(target = "users.id", source = "userId")
    Orders ordersToRequest(OrdersRequest request);

    @Mapping(target = "userId", source = "users.id")
    OrdersResponse toDTO(Orders orders);

    List<OrdersResponse> toDtoList(List<Orders> orders);

}

package products.order.service.impl;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import products.order.dao.UsersRepository;
import products.order.exception.ErrorCode;
import products.order.exception.NotFoundException;
import products.order.mapper.UsersMapper;
import products.order.model.requests.UsersRequest;
import products.order.model.responses.UsersResponse;
import products.order.service.UsersService;
import products.order.specification.UsersSpecification;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    public UsersServiceImpl(UsersRepository usersRepository, @Lazy PasswordEncoder passwordEncoder) {
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<UsersResponse> getUsers(UsersRequest request, Pageable pageable) {
        return UsersMapper.INSTANCE.toDtoPage(usersRepository.findAll(
                new UsersSpecification(request), pageable));
    }

    @Override
    public UsersResponse createUser(UsersRequest request) {
        request.setPassword(passwordEncoder.encode(request.getPassword()));
        var users = UsersMapper.INSTANCE.usersToRequest(request);
        return UsersMapper.INSTANCE.toDTO(usersRepository.save(users));
    }

    @Override
    public UsersResponse updateUser(UsersRequest request, Long id) {
        var users = usersRepository.findById(id).orElseThrow(
                () -> new NotFoundException(ErrorCode.RESOURCE_MISSING, "User not found")
        );
        if (Objects.nonNull(request.getPassword())) {
            request.setPassword(passwordEncoder.encode(request.getPassword()));
        }

        UsersMapper.INSTANCE.updateUsersFromRequest(request, users);
        return UsersMapper.INSTANCE.toDTO(usersRepository.save(users));
    }

    @Override
    public void deleteUser(Long id) {
        var user = usersRepository.findById(id).orElseThrow(NoSuchElementException::new);
        if (!ObjectUtils.isEmpty(user.getId())) {
            usersRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("User not found");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return usersRepository.findByUserName(username)
                .orElseThrow(() -> new UsernameNotFoundException("This username is invalid"));
    }
}

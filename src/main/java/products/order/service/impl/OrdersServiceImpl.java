package products.order.service.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import products.order.dao.OrdersRepository;
import products.order.dao.UsersRepository;
import products.order.exception.BadRequestException;
import products.order.exception.ErrorCode;
import products.order.exception.NotFoundException;
import products.order.mapper.OrdersMapper;
import products.order.model.OrderStatus;
import products.order.model.Role;
import products.order.model.filter.OrderFilter;
import products.order.model.requests.OrderActionRequest;
import products.order.model.requests.OrdersRequest;
import products.order.model.responses.OrdersResponse;
import products.order.service.OrdersService;
import products.order.specification.OrderSpecification;

@Service
@RequiredArgsConstructor
public class OrdersServiceImpl implements OrdersService {

    private final OrdersRepository ordersRepository;
    private final UsersRepository usersRepository;

    @Override
    public OrdersResponse createOrder(OrdersRequest request) {
        var user = usersRepository.findById(request.getUserId()).orElseThrow(
                () -> new NotFoundException(ErrorCode.RESOURCE_MISSING, "User not found"));

        var orders = OrdersMapper.INSTANCE.ordersToRequest(request);
        orders.getUsers().setId(request.getUserId());
        orders.setStatus(OrderStatus.ORDERED);

        if (user.getRole().equals(Role.EMPLOYEE)) {
            orders = ordersRepository.save(orders);
        } else {
            throw new BadRequestException(ErrorCode.UNEXPECTED_INTERNAL_ERROR, "You can not create order.");
        }
        return OrdersMapper.INSTANCE.toDTO(orders);
    }

    @Override
    public OrdersResponse receiverOrder(OrderActionRequest request, Long orderId) {
        var order = ordersRepository.findById(orderId).orElseThrow(
                () -> new NotFoundException(ErrorCode.RESOURCE_MISSING,
                        String.format("order not found orederId = %d", orderId)));

        var user = usersRepository.findById(request.getUserId()).orElseThrow(() -> new NotFoundException(
                ErrorCode.RESOURCE_MISSING, String.format("User not found userId = %d", request.getUserId())));

        if (request.getStatus().equals(OrderStatus.COMPLETED) && !order.getStatus().equals(OrderStatus.REJECT)) {
            if (user.getRole().equals(Role.USER)) {
                order.setOrderReceiverId(request.getUserId());
                order.setStatus(OrderStatus.COMPLETED);
            } else {
                throw new BadRequestException(ErrorCode.UNEXPECTED_INTERNAL_ERROR, "You can not completed");
            }

        } else if (request.getStatus().equals(OrderStatus.ORDERED)) {
            throw new BadRequestException(ErrorCode.UNEXPECTED_INTERNAL_ERROR, "you can not send ordered status");
        } else {
            if (user.getRole().equals(Role.USER)) {
                order.setOrderReceiverId(request.getUserId());
                order.setStatus(OrderStatus.REJECT);
            } else {
                throw new BadRequestException(ErrorCode.UNEXPECTED_INTERNAL_ERROR, "You can not reject");
            }

        }
        return OrdersMapper.INSTANCE.toDTO(ordersRepository.save(order));
    }

    @Override
    public List<OrdersResponse> getOrders(OrderFilter filter) {
        var order = ordersRepository.findAll(new OrderSpecification(filter));
        return OrdersMapper.INSTANCE.toDtoList(order);
    }
}

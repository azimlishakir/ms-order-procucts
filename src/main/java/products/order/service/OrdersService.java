package products.order.service;

import java.util.List;
import products.order.model.filter.OrderFilter;
import products.order.model.requests.OrderActionRequest;
import products.order.model.requests.OrdersRequest;
import products.order.model.responses.OrdersResponse;

public interface OrdersService {

    OrdersResponse createOrder(OrdersRequest request);

    OrdersResponse receiverOrder(OrderActionRequest request, Long orderId);

    List<OrdersResponse> getOrders(OrderFilter filter);
}

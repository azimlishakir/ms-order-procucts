package products.order.service;

import org.springframework.data.domain.Pageable;
import products.order.model.requests.UsersRequest;
import products.order.model.responses.UsersResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UsersService extends UserDetailsService {

    List<UsersResponse> getUsers(UsersRequest request, Pageable pageable);

    UsersResponse createUser(UsersRequest request);

    UsersResponse updateUser(UsersRequest request, Long id);

    void deleteUser(Long id);
}

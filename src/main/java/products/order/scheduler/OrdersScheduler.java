package products.order.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import products.order.model.OrderStatus;
import products.order.model.filter.OrderFilter;
import products.order.service.OrdersService;

@Component
@Slf4j
@EnableScheduling
@RequiredArgsConstructor
public class OrdersScheduler {

    private final OrdersService ordersService;

    @Scheduled(cron = "0 */15 * * * * ", zone = "GMT+4")
    private void getOrders() {
        var order = ordersService.getOrders(OrderFilter.builder()
                .status(OrderStatus.ORDERED)
                .build());
        order.forEach(ordersResponse -> {
            log.info("id = ".concat(ordersResponse.getId().toString()));
            log.info("comment = ".concat(ordersResponse.getComment()));
            log.info("status = ".concat(ordersResponse.getStatus().name()));
            log.info("userId = ".concat(ordersResponse.getUserId().toString()));
            log.info("createdAt = ".concat(ordersResponse.getCreatedAt().toString()));
        });
    }
}

package products.order.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import products.order.dao.model.Orders;

public interface OrdersRepository extends JpaRepository<Orders, Long>,
        JpaSpecificationExecutor<Orders> {
}

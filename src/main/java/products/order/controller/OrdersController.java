package products.order.controller;


import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import products.order.model.filter.OrderFilter;
import products.order.model.requests.OrderActionRequest;
import products.order.model.requests.OrdersRequest;
import products.order.model.responses.OrdersResponse;
import products.order.service.OrdersService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrdersController {

    private final OrdersService ordersService;

    @PostMapping
    public ResponseEntity<OrdersResponse> createOrder(@RequestBody OrdersRequest request) {
        return ResponseEntity.ok(ordersService.createOrder(request));
    }

    @GetMapping
    public ResponseEntity<List<OrdersResponse>> getOrders(OrderFilter filter) {
        return ResponseEntity.ok(ordersService.getOrders(filter));
    }

    @PutMapping("/{orderId}")
    public ResponseEntity<OrdersResponse> receiverOrder(@RequestBody OrderActionRequest request,
                                                        @PathVariable Long orderId) {
        return ResponseEntity.ok(ordersService.receiverOrder(request, orderId));
    }

}

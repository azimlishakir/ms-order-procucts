package products.order.controller;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import products.order.model.requests.UsersRequest;
import products.order.model.responses.UsersResponse;
import products.order.service.UsersService;


@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping
    public ResponseEntity<List<UsersResponse>> getUsers(UsersRequest request, Pageable pageable) {
        return ResponseEntity.ok(usersService.getUsers(request, pageable));
    }

    @PostMapping
    public ResponseEntity<UsersResponse> createUser(@RequestBody UsersRequest request) {
        return ResponseEntity.ok(usersService.createUser(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UsersResponse> updateUser(@RequestBody UsersRequest request,
                                                    @PathVariable("id") Long id) {
        return ResponseEntity.ok(usersService.updateUser(request, id));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteUser(@PathVariable("id") Long id) {
        usersService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

}

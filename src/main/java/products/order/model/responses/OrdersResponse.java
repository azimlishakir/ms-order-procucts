package products.order.model.responses;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import products.order.model.OrderStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdersResponse {

    private Long id;
    private String comment;
    private Long userId;
    private OrderStatus status;
    private LocalDateTime createdAt;
}

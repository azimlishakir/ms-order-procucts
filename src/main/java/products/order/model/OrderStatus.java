package products.order.model;

public enum OrderStatus {
    ORDERED, COMPLETED, REJECT
}

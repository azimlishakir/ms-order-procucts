package products.order.model.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import products.order.model.Role;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsersRequest {
    @JsonIgnore
    private Long id;
    private String name;
    private String lastName;
    private String userName;
    private String password;
    private Role role;
}

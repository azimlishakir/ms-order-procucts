package products.order.specification;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;
import products.order.dao.model.Orders;
import products.order.model.filter.OrderFilter;

public class OrderSpecification implements Specification<Orders> {

    private final OrderFilter orderFilter;

    public OrderSpecification(OrderFilter orderFilter) {
        this.orderFilter = orderFilter;
    }

    @Override
    public Predicate toPredicate(Root<Orders> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        if (!ObjectUtils.isEmpty(orderFilter.getStatus())) {
            predicates.add(criteriaBuilder.equal(root.get("status"),
                    orderFilter.getStatus()));
        }

        if (!ObjectUtils.isEmpty(orderFilter.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"),
                    orderFilter.getId()));
        }

        if (!ObjectUtils.isEmpty(orderFilter.getDateFrom())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"),
                    LocalDateTime.of(orderFilter.getDateFrom(), LocalTime.of(0, 0))));
        }

        if (!ObjectUtils.isEmpty(orderFilter.getDateTo())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("createdAt"),
                    LocalDateTime.of(orderFilter.getDateTo(), LocalTime.of(0, 0))));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}

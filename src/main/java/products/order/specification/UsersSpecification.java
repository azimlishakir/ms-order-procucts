package products.order.specification;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;
import products.order.dao.model.Users;
import products.order.model.requests.UsersRequest;

public class UsersSpecification implements Specification<Users> {

    private final UsersRequest request;

    public UsersSpecification(UsersRequest request) {
        this.request = request;
    }

    @Override
    public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

        if (!ObjectUtils.isEmpty(request.getId())) {
            predicates.add(criteriaBuilder.equal(root.get("id"),
                    request.getId()));
        }

        if (!ObjectUtils.isEmpty(request.getName())) {
            predicates.add(criteriaBuilder.equal(root.get("name"),
                    request.getName()));
        }

        if (!ObjectUtils.isEmpty(request.getLastName())) {
            predicates.add(criteriaBuilder.equal(root.get("lastName"),
                    request.getLastName()));
        }

        if (!ObjectUtils.isEmpty(request.getUserName())) {
            predicates.add(criteriaBuilder.equal(root.get("userName"),
                    request.getUserName()));
        }

        if (!ObjectUtils.isEmpty(request.getRole())) {
            predicates.add(criteriaBuilder.equal(root.get("role"),
                    request.getRole()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}

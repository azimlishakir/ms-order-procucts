# ms-order-procucts

## how to set up ms-order-procucts

1. ```docker-compose -f docker-compose.yml up -d```
2. Application runs in the IDE

Firstly, the user gets a token for different service calls. For this, the user should call the following endpoint.


```
POST http://localhost:8080/login
```

## USERS

Then we use the following endpoint to create a new user.I have an opinion that if you don't want to create a new user, you can use the default user when you run the application, which will create a new user by Liquibase for you.

```
POST http://localhost:8080/users
```

If we want to get all users with a filter, We will use the following endpoint

You can use following filter id,name,lastName, userName, role

```
GET http://localhost:8080/users
```

We can update a user following an endpoint.

```
    PUT http://localhost:8080/users/{id}
```

We can delete a user by following the endpoint.

```
    DELETE http://localhost:8080/users/{id}
```

## ORDERS

We can create an order by following the endpoint.
If our user's role is employee, we will create an order.


```
    POST localhost:8080/orders
```

if we want to get all orders, we will use following endpoint.

We can use following filter
id
status
dateFrom
dateTo

```
    GET localhost:8080/orders
```

The user can completed or reject the order. If the user role is user, the user will be able to complete or reject
User can call the following endpoint for receiver order.
We have a scheduler. This scheduler checks every 15 minutes when we created  an order, we  will print new orders list on the screen.it is run automatically


```
    PUT localhost:8080/orders/{orderId}
```

You can get request information from the resources/requests.I added.http files and a postman collection.
***

